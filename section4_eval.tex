
\section{Evaluation}

\begin{figure}[t]
	\centering
    \begin{subfigure}{0.5\columnwidth}
        \centering
        \includegraphics[width=0.95\columnwidth]{figures/blur_1_exact.png}
        \caption{precise}
    \end{subfigure}%
    ~ 
    \begin{subfigure}{0.5\columnwidth}
        \centering
        \includegraphics[width=0.95\columnwidth]{figures/blur_1_approx.png}
        \caption{approximate}
    \end{subfigure}
    \caption{Comparison of precise and approximate blur filter outputs (replacing 40\% of global memory accesses).\vspace{-0.1in}}
    \label{figure:compare}
\end{figure}

\begin{figure}[t]
    \centering
    \includegraphics[trim = 0.75in 4.225in 0.75in 4.225in, clip, width=3.15in]{figures/runtime.pdf}
    \caption{Runtime using texture approximations, normalized to precise execution baseline.\vspace{-0.1in}}
    \label{figure:runtime}
\end{figure}

\begin{figure}[t]
    \centering
    \includegraphics[trim = 0.75in 4.225in 0.75in 4.225in, clip, width=3.15in]{figures/error-2.pdf}
    \caption{Per-frame error using texture approximations for 40\% of global memory accesses.\vspace{-0.1in}}
    \label{figure:error}
\end{figure}

As a proof of concept that texture cache approximation can be successful applied on real hardware, we explore its use in a blur kernel derived from the San Diego CV Benchmark Suite \cite{Venkata-iiswc09}, executing on an NVIDIA 780GTX. 
Our input was 16 HD frames (resolution 1920$\times$1080), taken from a short stock film clip \cite{ladybug}.
To obtain the kernel's execution time, we use the NVIDIA CUDA Toolkit Profiler~\cite{profiler}, and obtain the number of cycles the kernel executed on an SM. % in the ``elapsed\_cycles\_sm'' metric.
Figure~\ref{figure:runtime} displays the execution time (normalized to a precise kernel) of our approximate blur kernel, varying the number of texture cache approximations, while Figure~\ref{figure:error} shows the per-frame error when we use the training set generated from the first frame to process all 16 images. 
We used mean pixel difference across all 3 RGB channels as our error metric, which has been used in prior work on approximation-induced image error~\cite{SAGE}. 

By replacing 40\% of the global memory accesses with texture cache approximations, we obtain a 12\% decrease in kernel execution time, with a mean pixel error of 0.4\%.
This corresponds to a maximum mean pixel difference of 1.1 out of a maximum 8-bit channel value of 255.
We observe errors of 0.1\% and 1.3\% when replacing 20\% and 60\% of memory accesses respectively.
The average texture cache hit rate exceeds 99\%, demonstrating that our training set is small enough to reside in the 12kB texture cache of each SM while still resulting in accurate images.
Figure~\ref{figure:compare}  compares the visual output of precise and approximate blur kernels to show that our technique generates output that appears consistent with precise kernel execution.

Since we use the same training set for all 16 frames, the mean pixel error increases for frames further away from the frame used to generate the training set as shown in Figure~\ref{figure:error}. 
Therefore, a real-time algorithm would necessitate re-training when the output error exceeds a certain threshold. 
Generating the new training sets could be easily overlapped with already executing kernels to avoid stalling the GPU. 

Speedup is also possible by simply using a blur kernel that has less coefficients, without using our texture cache approach.
However, depending on the application, it is possible that the output would change by a much greater degree than the slight errors in accuracy conceded by using value approximation.
%\nej{In your presentation you say that outputs could change noticeably? If so, why not say that here}
Additionally, this approach would not be valid for a more complex access pattern, such as those appearing in computational fluid dynamics, where many differential equations must be solved for each unit volume in the region of interest.
Hardware value approximation is in fact harmonious with reduced precision algorithms, and ultimately the choice to use one instead of the other depends on the application and the goals of the systems designer.

\begin{comment}
We also observe that using NVIDIA's \emph{surface} API, the approximation data can be updated \emph{online}, eliminating the need for training sets entirely. 
However, surfaces do not support linear interpolation between indices, and have the drawback of flushing the texture cache upon writing to surface memory~\cite{texcache_ieeemicro}.
This solution avoids introducing a cache coherence protocol between SM's in the GPU; it would also reduce our kernel speedup as unmodified lines now must be reinstalled into the texture cache, even though they were not updated.
These factors make surfaces an unattractive means to update the approximation data online.
\end{comment}
%\nej{One reviewer asked about hardware vs software -- you should have some discussion of this somewhere in the paper.  Couldn't one just use a different blur to get speedup?}
