# /bin/bash
FILEDIR=$1
for i in $FILEDIR/*.csv; do
    BASE=$(basename $i)
    gnuplot -e "I_NAME='$i'; O_NAME='$FILEDIR/${BASE}_ebreak.eps';" plotEBreak_Sep.scr
done
